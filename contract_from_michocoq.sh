#!/bin/bash

set -e
set -o pipefail

COQ_IDENTIFIER="contract_s"

if [[ -z "$1" || -z "$2" ]]; then
    echo "Usage: $0 <coq_string_file.v> <contract.tz> [<COQ_IDENTIFIER>]"
    echo
    echo "Loads <coq_string_file.v>, prints the definition <COQ_IDENTIFIER>,"
    echo "(\`$COQ_IDENTIFIER\` by default)"
    echo "massages the string into something that parse as Michelson."
    echo "Then pretty prints the resulting Michelson, and compares it with the"
    echo "pretty printed version of <contract.tz>"
    echo ""
    exit 1
fi

if [[ ! -z "$3" ]]; then
    COQ_IDENTIFIER="$3"
fi

if ! command -v tezos-client > /dev/null; then
    echo "No tezos-client, start sandbox?"
    exit 1
fi


if ! command -v coqtop > /dev/null; then
    echo "No coqtop, correct switch?"
    exit 1
fi


COQ_STRING_FILE=$1
TZ_FILE=$2

tmp1=`mktemp`

cd `dirname $COQ_STRING_FILE`
coqtop -l $(basename $COQ_STRING_FILE) <<< "Eval cbv in $COQ_IDENTIFIER." 2>/dev/null > $tmp1
cd -



COQ_MICHELSON_SCRIPT=`mktemp`
tail -n+2 $tmp1 \
    | head -n -1 \
    | sed 's/"%string//' \
    | sed '1 s/.* = "//' \
    | sed 's/""/"/g' \
    | tr "\n" " " > $COQ_MICHELSON_SCRIPT

NORMALIZED_COQ_MICHELSON_SCRIPT=`mktemp`

HASH_TZ_FILE=$(tezos-client hash script $TZ_FILE)
echo "Unnormalized hash of $TZ_FILE: $HASH_TZ_FILE"

echo $COQ_MICHELSON_SCRIPT
HASH_COQ_MICHELSON_SCRIPT=$(tezos-client hash script $COQ_MICHELSON_SCRIPT)
echo "Unnormalized hash of $COQ_IDENTIFIER in $COQ_STRING_FILE: $HASH_COQ_MICHELSON_SCRIPT"

if [[ "$HASH_TZ_FILE" = "$HASH_COQ_MICHELSON_SCRIPT" ]]; then
    echo "Yes, they are the same"
else
    echo "!! They are the not same!"
fi


tezos-client convert script $TZ_FILE from michelson to michelson > $tmp1
tezos-client convert script $COQ_MICHELSON_SCRIPT from michelson to michelson > $NORMALIZED_COQ_MICHELSON_SCRIPT

HASH_COQ_MICHELSON_SCRIPT_NORMALIZED=$(tezos-client hash script $tmp1)
echo "Normalized hash of $TZ_FILE: $HASH_COQ_MICHELSON_SCRIPT_NORMALIZED"

HASH_NORMALIZED_COQ_MICHELSON_SCRIPT=$(tezos-client hash script $NORMALIZED_COQ_MICHELSON_SCRIPT)
echo "Normalized hash of $COQ_IDENTIFIER in $COQ_STRING_FILE: $HASH_NORMALIZED_COQ_MICHELSON_SCRIPT"

if [[ "$HASH_COQ_MICHELSON_SCRIPT_NORMALIZED" = "$HASH_NORMALIZED_COQ_MICHELSON_SCRIPT" ]]; then
    echo "Yes, they are the same"
else
    echo "!! They are the not same!"
fi

if diff --brief $tmp1 $NORMALIZED_COQ_MICHELSON_SCRIPT; then
    echo 'The contracts are the same modulo whitespace.'
else
    diff $tmp1 $NORMALIZED_COQ_MICHELSON_SCRIPT
fi

rm $tmp1 $COQ_MICHELSON_SCRIPT $NORMALIZED_COQ_MICHELSON_SCRIPT
