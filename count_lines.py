import sys
import re


def count_proof_lines(coq_file):
    in_lemma = None
    try:
        for i, l in enumerate(coq_file):
            m_prop = re.search('(Lemma|Fact|Property|Theorem) (\w+)', l)
            if re.search('Proof..*Qed.', l):
                assert in_lemma is not None, \
                    "Proof..Qed outside property"
                in_lemma['count'] = 1
                print(f"{in_lemma['prop'][0]} {in_lemma['prop'][1]}\t{in_lemma['count']}")
                in_lemma = None
            elif re.search('Qed.', l):
                assert in_lemma is not None, \
                    "Qed outside property"
                print(f"{in_lemma['prop'][0]} {in_lemma['prop'][1]}\t{in_lemma['count']}")
                # print("QED")
                in_lemma = None
            elif m_prop:
                assert in_lemma is None, \
                    "Does not support nested proofs"
                in_lemma = {'prop': m_prop.groups(), 'in_proof': False, 'count': 0}
                # print(in_lemma)
            elif re.search('Proof.', l):
                assert in_lemma is not None, \
                    "Proof. outside property"
                in_lemma['in_proof'] = True
            elif in_lemma is not None and in_lemma['in_proof']:
                in_lemma['count'] = in_lemma['count'] + 1
                # print(in_lemma['count'])
    except AssertionError as e:
        print(re.search('Proof..*Qed.', l))
        raise Exception(f"Error at line {i + 1}: '{l}'") from e


def count_proof_lines2(coq_file):
    in_lemma = None
    try:
        for i, l in enumerate(coq_file):
            m_prop = re.search('(Lemma|Fact|Property|Theorem) (\w+)', l)
            if m_prop:
                assert in_lemma is None, \
                    "Does not support nested proofs"
                in_lemma = {'prop': m_prop.groups(), 'count': 1}
            elif re.search('Qed.', l):
                assert in_lemma is not None, \
                    "Qed outside property"
                in_lemma['count'] = in_lemma['count'] + 1
                print(f"{in_lemma['prop'][0]} {in_lemma['prop'][1]}\t{in_lemma['count']}")
                # print("QED")
                in_lemma = None
            elif in_lemma is not None:
                in_lemma['count'] = in_lemma['count'] + 1
                # print(in_lemma['count'])
    except AssertionError as e:
        print(re.search('Proof..*Qed.', l))
        raise Exception(f"Error at line {i + 1}: '{l}'") from e

if __name__ == '__main__':
    for p in sys.argv[1:]:
        with open(p) as f:
            count_proof_lines2(f)
