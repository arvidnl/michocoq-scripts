import argparse
import string
import sys
import re
from typing import Union, Tuple, Any
from copy import deepcopy
from io import TextIOBase


import pytezos  # type: ignore
import jq  # type: ignore
from pytezos.michelson.converter import micheline_to_michelson, michelson_to_micheline  # type: ignore


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


def prim_like(mich, prim):
    return [(f"[{i}]", p) for (i, p) in enumerate(mich) if p["prim"] == prim]


def first_prim_like(mich, prim):
    return prim_like(mich, prim)[0]


def find_branching_point(path, code_seq):
    assert isinstance(
        code_seq, list
    ), "Expects contract code to be a list (SEQ)"

    for (idx, node) in enumerate(code_seq):
        if node["prim"] == "IF_LEFT":
            return (path + f"[{idx}]", node)

    return None


def find_eps(path, code):
    # assert len(code) == 1
    assert isinstance(code, list)
    if code[0]["prim"] == "IF_LEFT" and len(code) == 1:
        path = path + "[0].args"
        return find_eps(path + "[0]", code[0]["args"][0]) + find_eps(
            path + "[1]", code[0]["args"][1]
        )

    return [(path, code)]


def find_eps0(path, code):
    # assert len(code) == 1
    assert isinstance(code, dict)
    assert code["prim"] == "IF_LEFT"

    path = path + ".args"
    return find_eps(path + "[0]", code["args"][0]) + find_eps(
        path + "[1]", code["args"][1]
    )


def get_code(mich):
    (path, code) = first_prim_like(mich, "code")
    return ("." + path + ".args[0]", code["args"][0])


def split(script: TextIOBase):
    text = script.read()
    mich = michelson_to_micheline(text)
    epss = pytezos.michelson.contract.Contract.from_michelson(
        text
    ).parameter.entries()
    (path, code) = get_code(mich)
    eprint(f"Code at: {path}")

    (path, brst) = find_branching_point(path, code)
    eprint(f"Branching point at: {path}")

    assert brst is not None, "Could not find branching point."

    ep_codes = find_eps0(path, brst)
    assert len(epss) == len(
        ep_codes
    ), f"Found {len(ep_codes)} entrypoints but expected {len(epss)}"

    template = mich
    entrypoints = []
    for (idx, ((name, _), (path, ep))) in enumerate(zip(epss, ep_codes)):
        eprint(f"Entrypoint {idx}, `{name}` (@ {path}):")
        eprint("by hand:" + micheline_to_michelson(ep)[:50])

        placeholder_name = "EP_PLH_" + name

        jq.compile(path)
        ep_from_jq = jq.compile(path).input(mich).first()
        eprint("From jq: " + micheline_to_michelson(ep_from_jq)[:50])
        assert ep == ep_from_jq

        entrypoints.append(
            {"name": name, "placeholder_name": placeholder_name, "code": ep}
        )

        template = (
            jq.compile(path + f' = {{prim: "${placeholder_name}"}}')
            .input(template)
            .first()
        )

    eprint(micheline_to_michelson(template))

    # make storage getters / setters
    try:
        storage = [n["args"][0] for n in mich if n["prim"] == "storage"][0]
    except IndexError:
        eprint("Could not find the storage")
        raise IndexError

    return {
        "full": mich,
        "entrypoints": entrypoints,
        "template": template,
        "storage": storage,
    }


def strip_annotations(n):
    n.pop("annots", None)
    if "args" in n:
        n["args"] = list(map(strip_annotations, n["args"]))
    return n


def storage_to_tuple(n, idx=0):
    # print(f"Tuplifying {n}")
    if n["prim"] == "pair":
        (br1, idx1) = storage_to_tuple(n["args"][0], idx)
        (br2, idx2) = storage_to_tuple(n["args"][1], idx1)
        return ((br1, br2), idx2)

    assert len(n["annots"]) == 1
    name = n["annots"][0][1:]
    n1 = strip_annotations(deepcopy(n))
    ty = micheline_to_michelson(n1)
    return ({"name": name, "type": ty, "idx": idx}, idx + 1)


def pp_storage_tuple(sto):
    if isinstance(sto, tuple):
        return "(%s, %s)" % tuple(map(pp_storage_tuple, sto))
    if isinstance(sto, dict):
        return f"{sto['name']}"
    assert False, f"unexpected type: {type(sto)}"
    return None


StorageTuple = Union[Tuple[Any, Any], dict]


def rename_storage_tuple(
    sto: StorageTuple, needle: int, name: str
) -> StorageTuple:
    if isinstance(sto, tuple):
        sto1 = rename_storage_tuple(sto[0], needle, name)
        sto2 = rename_storage_tuple(sto[1], needle, name)
        return (sto1, sto2)
    if isinstance(sto, dict):
        if sto["idx"] == needle:
            sto["name"] = name
        return sto

    assert False, f"unexpected type: {type(sto)}"
    return None


def storage_tuple_leaves(sto):
    if isinstance(sto, tuple):
        ns1 = storage_tuple_leaves(sto[0])
        ns1.extend(storage_tuple_leaves(sto[1]))
        return ns1
    if isinstance(sto, dict):
        return [sto]

    assert False, f"unexpected type: {type(sto)}"
    return None


def storage_accessors(tup: StorageTuple):
    # assert annotations are unique


    # adds parens if type contains space
    def pp_typ(ty):
        return f"({ty})" if re.search(' ', ty) else  ty

    for e in storage_tuple_leaves(tup):
        print(
            f"Definition sto_{e['name']} (sto : data storage_ty) : data {pp_typ(e['type'])} :=\n"
            + f"   match sto with\n"
            + f"      | {pp_storage_tuple(tup)} =>\n"
            + f"        {e['name']}\n"
            + f"   end.\n"
        )

    print("\n\n")

    for e in storage_tuple_leaves(tup):
        tup_renamed = rename_storage_tuple(
            deepcopy(tup), e["idx"], e["name"] + "'"
        )
        print(
            f"Definition sto_set_{e['name']} (sto : data storage_ty) ({e['name']}' : data {pp_typ(e['type'])}) : data storage_ty :=\n"
            + f"   match sto with\n"
            + f"      | {pp_storage_tuple(tup)} =>\n"
            + f"        {pp_storage_tuple(tup_renamed)}\n"
            + f"   end.\n"
        )

    # => (("a", tya), (("b", tyb), ("c", tyc)))
    # => dfs, for each element e, ety, eidx print getter and setter:
    # => Definition sto_get_{e} : (sto : data storage_ty) : {ety} :=
    # =>   match sto with | {destruct_storage} => {e}
    # => Definition sto_set_{e} : (sto : data storage_ty) : {data storage_ty} :=
    # =>   match sto with | {destruct_storage} => {destruct_storage (rename sto eidx e'}


def accessors(script: TextIOBase):
    text = script.read()
    mich = michelson_to_micheline(text)
    storage = [n["args"][0] for n in mich if n["prim"] == "storage"][0]
    (tup, _) = storage_to_tuple(storage)

    # print(pp_storage_tuple(tup))
    # print(pp_storage_tuple(rename_storage_tuple(tup, 0, 'FOO')))
    # print(pp_storage_tuple(rename_storage_tuple(tup, 1, 'FOO')))
    # print(pp_storage_tuple(rename_storage_tuple(tup, 2, 'FOO')))

    storage_accessors(tup)

def coq_quote(s):
    return s.replace('"', '""')

def coq_with_header(body: str):
    with open("coq_header.v") as header:
        source = header.read()
        source += body
        return source


def format_to_coq(parsed):
    source = "Require Import String.\n\n"

    for i in parsed["entrypoints"]:
        ep_formatted = coq_quote(micheline_to_michelson(i["code"]))
        name = i["name"]
        source += f'Definition ep_{name} : string := "{ep_formatted}".\n\n'

    template_formatted = string.Template(
        coq_quote(micheline_to_michelson(parsed["template"]))
    )
    full_concat = template_formatted.safe_substitute(
        {
            i["placeholder_name"]: f"\" ++ ep_{i['name']} ++ \""
            for i in parsed["entrypoints"]
        }
    )

    source += f'Definition contract_s : string := "{full_concat}".'

    print(coq_with_header(source))

def to_coq_string(script: TextIOBase):
    source = "Require Import String.\n\n"

    script_text = script.read()
    script_quoted = coq_quote(script_text)
    source += f'Definition main : string := "{script_quoted}".\n'

    print(coq_with_header(source))

def main():
    parser = argparse.ArgumentParser(
        description="Manipulation of Michelson scripts for Mi-Cho-Coq."
    )
    parser.add_argument(
        "action", type=str, help="action", choices=["split", "accessors", "to-coq-string"]
    )
    parser.add_argument(
        "script",
        type=argparse.FileType("r"),
        help="Michelson script",
    )

    args = parser.parse_args()

    if args.action == "split":
        format_to_coq(split(args.script))
    elif args.action == "accessors":
        accessors(args.script)
    elif args.action == "to-coq-string":
        to_coq_string(args.script)

if __name__ == "__main__":
    main()
