#!/bin/bash

set -e
set -o pipefail

TEZOS_CLIENT_PATH=~/dev/nomadic-labs/tezos/master/tezos-client
COQ_IDENTIFIER="contract_s"

if [[ -z "$1" ]]; then
    echo "Usage: $0 <coq_string_file.v> [<COQ_IDENTIFIER>]"
    echo
    echo "Loads <coq_string_file.v>, prints the definition <COQ_IDENTIFIER>,"
    echo "(\`$COQ_IDENTIFIER\` by default)"
    echo "massages the string into something that parse as Michelson."
    echo "Then hashes the resulting script."
    echo ""
    exit 1
fi

if [[ ! -z "$2" ]]; then
    COQ_IDENTIFIER="$2"
fi

# if ! command -v tezos-client > /dev/null; then
#     echo "No tezos-client, start sandbox?"
#     exit 1
# fi


if ! command -v coqtop > /dev/null; then
    echo "No coqtop, correct switch?"
    exit 1
fi


if [ ! -f "$TEZOS_CLIENT_PATH" ]; then
    echo "No tezos-client at '$TEZOS_CLIENT_PATH', try setting the \$TEZOS_CLIENT_PATH variable."
    exit 1
fi

# set up mockup client
BASE_DIR=$(mktemp -d -t tezos-tmp-hash-contracts.XXXXXXXX)
function client() {
    $TEZOS_CLIENT_PATH --protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK --mode mockup --base-dir $BASE_DIR "$@"
}
client create mockup

COQ_STRING_FILE=$1

tmp1=`mktemp`

cd `dirname $COQ_STRING_FILE`
coqtop -l $(basename $COQ_STRING_FILE) <<< "Eval cbv in $COQ_IDENTIFIER." 2>/dev/null > $tmp1
cd -


COQ_MICHELSON_SCRIPT=`mktemp`
tail -n+2 $tmp1 \
    | head -n -1 \
    | sed 's/"%string//' \
    | sed '1 s/.* = "//' \
    | sed 's/""/"/g' \
    | tr "\n" " " > $COQ_MICHELSON_SCRIPT

NORMALIZED_COQ_MICHELSON_SCRIPT=`mktemp`

echo $COQ_MICHELSON_SCRIPT
HASH_COQ_MICHELSON_SCRIPT=$(client hash script $COQ_MICHELSON_SCRIPT)
echo "Hash of $COQ_IDENTIFIER in $COQ_STRING_FILE: $HASH_COQ_MICHELSON_SCRIPT"
