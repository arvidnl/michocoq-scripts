This repo contains two scripts for working with michocoq developments:


# `split.py`

Split is used to bootstrap or update a smart contract verification
development for mi-cho-coq. It has two main functionalities:

 - Splitting: reads a Michelson contract scripts and split it in a
   different Coq strings that correposponds to individual entrypoints,
   as well as a string concatenating the individual entrypoints to
   obtain the full script as a coq string.

 - Storage accessors: reads the storage type of a Michelson contract
   script and generates Coq definitions corresponding to
   getters/setters of that type

## Installation

In theory, handles dependencies using poetry and so is installable with

     poetry install

It also requires `jq` in the path.

## Usage

### Splitting a script:

    $ poetry run python split.py split michocoq/src/contracts/vesting_tez.tz

    (* Open Source License *)
    (* Copyright (c) 2021 Nomadic Labs. <contact@nomadic-labs.com> *)
    < ... >
    (* DEALINGS IN THE SOFTWARE. *)
    
    Require Import String.
    
    Definition ep_setDelegate : string := "{ SWAP ;
      DUP ;
      DIP { CAR ; CDR ; SENDER ; COMPARE ; EQ ; IF { DIP { NIL operation } ; SET_DELEGATE ; CONS } { FAILWITH } } ;
      SWAP ;
      PAIR }".
    
    Definition ep_vest : string := "{ PAIR ;
      DUP ;
     < ... >
        { SWAP ; PUSH mutez 1 ; MUL ; UNIT ; TRANSFER_TOKENS ; DIP { NIL operation } ; CONS } ;
      DIP { PAIR } ;
      PAIR }".
    
    Definition contract_s : string := "parameter (or (option %setDelegate key_hash) (nat %vest));
    storage (pair (pair %wrapped (address %target) (address %delegateAdmin))
                  (pair (nat %vested)
                        (pair %schedule (timestamp %epoch)
                                        (pair (nat %secondsPerTick) (nat %tokensPerTick)))));
    code { DUP ; CAR ; DIP { CDR } ; IF_LEFT " ++ ep_setDelegate ++ " " ++ ep_vest ++ " }".

### Accessors:

    $ poetry run python split.py accessors michocoq/src/contracts/vesting_tez.tz
    Definition sto_target (sto : data storage_ty) : data address :=
       match sto with
          | ((target, delegateAdmin), (vested, (epoch, (secondsPerTick, tokensPerTick)))) =>
            target
       end.
    < ... >

     
# `contract_from_michocoq.sh`

This script does in some sense the inverse of `split.py`.  It's meant
to get some assurance that the script in a Coq file corresponds to a
given Michelson script file.

The script takes a Coq .v file name, a Michelson script .tz file name
and a coq identifier.  It loads the Coq file and prints the
identifier. It massages the output into a Michelson script, normalizes
it and compares it with the normalized version of the Michelson script
given as argument.

For example, to verify that the Coq version of the CPMM included in
the branch `arvid@cpmm2-verification` is the same as the script file
in that same branch:

It requires a `tezos-client` accessible under that name. It requires
that the file containg the coq string is readable by coqtop.

Example usage:

    $ ./contract_from_michocoq.sh ../arvid@cpmm2-verification/src/contracts_coq/dexter_string.v ../arvid@cpmm2-verification/src/contracts/dexter2/dexter.liquidity_baking.mligo.tz contract_s
    /home/arvid/dev/nomadic-labs/mi-cho-coq/scripts
    The contracts are the same modulo whitespace.


If not the case:

    $ ./contract_from_michocoq.sh ../arvid@cpmm2-verification/src/contracts_coq/dexter_string_with_error.v ../arvid@cpmm2-verification/src/contracts/dexter2/dexter.liquidity_baking.mligo.tz contract_s
    /home/arvid/dev/nomadic-labs/mi-cho-coq/scripts
    Files /tmp/tmp.6Dk71ZnNfp and /tmp/tmp.vFitcNOU5l differ
    185c185
    <                                     CONTRACT %transfer (pair address (pair address nat)) ;
    ---
    >                                     CONTRACT %transfer_boop (pair address (pair address nat)) ;
